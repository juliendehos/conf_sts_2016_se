WWW_ROOT = ~/public-html/conf_sts_2016_se

.PHONY: all publish clean

all: $(WWW_ROOT)
	$(MAKE) -C slides WWW_ROOT=$(WWW_ROOT)
	$(MAKE) -C www WWW_ROOT=$(WWW_ROOT)

$(WWW_ROOT):
	mkdir -p $(WWW_ROOT)

publish: all
	scp -r $(WWW_ROOT) yangra.univ-littoral.fr:public-html/

clean:
	rm -rf $(WWW_ROOT)
	$(MAKE) -C slides clean
	$(MAKE) -C www clean

