with import <nixpkgs> {}; 

stdenv.mkDerivation {
    name = "exrview";
    buildInputs = [ stdenv texlive.combined.scheme-full graphviz inkscape ];
    src = ./.;
    installPhase = "mkdir -p $out; cp conf_sts_2016_se.pdf $out";
}

