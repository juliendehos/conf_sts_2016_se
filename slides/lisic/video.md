# traitement video avec ffmpeg/avconv

## changer la luminosité/contraste

```
avconv -i oculiste.mp4 -filter:v lutyuv="y=gammaval(0.8)" o.mp4
```
ou
```
avconv -i oculiste.mp4 -vf lutyuv="y=val*2" o.mp4
```

## extraire une frame

```
avconv -ss 0.5 -i oculiste.mp4 -t 1 oculiste.png
```

## faire une video à partir d'images (slideshow)

```
avconv -loop 1 -r 2 -i cornellbox_stereo_1_%1d.png -c:v libx264 -t 30 -s 400x300 -sws_flags neighbor -r 15 cornellbox_stereo_1.mp4
```
ou
```
avconv -loop 1 -r 2 -i cornellbox_stereo_%1d.png -c:v libx264 -t 30 -sws_flags neighbor -r 15 cornellbox_stereo.mp4
```

