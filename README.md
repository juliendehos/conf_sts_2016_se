
*Conférence du Pôle STS de l'ULCO du mardi 7 juin 2016*

# Calculer des images de synthèse

*ou comment Nicholas Metropolis joue à la roulette russe à Monte-Carlo*

Les images de synthèse sont des images calculées par ordinateur à partir d’une
scène virtuelle. Elles ont l’avantage, par rapport aux photographies, de
pouvoir représenter facilement toutes sortes de scènes, éventuellement
complètement imaginaires ou irréalisables en pratique. C’est pourquoi les
images de synthèse sont utilisées dans de nombreux domaines : cinéma, design
industriel, publicité, architecture... Par exemple, les catalogues de certains
fabricants de meubles sont composés majoritairement d’images de synthèse.

Calculer des images de synthèse réalistes est un problème difficile : les
données à manipuler sont souvent volumineuses et complexes, et le transport de
la lumière n’est pas trivial. Il est donc irréalisable de calculer
explicitement comment toute la lumière se propage dans la scène virtuelle. Les
algorithmes de synthèse classiques utilisent des méthodes probabilistes
(Monte-Carlo, Chaîne de Markov, Metropolis...) pour estimer l’image finale, en
privilégiant les rayons de lumières les plus importants.

Le but de cet exposé est de donner un aperçu de la synthèse d’images et du
problème du transport de la lumière, puis de détailler quelques méthodes de
résolution et quelques travaux du LISIC dans ce domaine.

![](data/conf_sts_2016_se.mp4)

- Vidéo : [webm](data/conf_sts_2016_se.webm), [mp4](data/conf_sts_2016_se.mp4)
- [Slides en PDF](data/conf_sts_2016_se_final.pdf) 
- [Annonce sur la page du LISIC](http://www-lisic.univ-littoral.fr/spip.php?article305)
- [Podcast ULCO](http://podcast.univ-littoral.fr/index.php?pagi=3&page=aff&disc=25&art=6504&id_case=51AEF648-BD11-4D91-B189-B23312528834)

